# Code Challenge

## Overview

The goal is to evaluate the familiarity and the knowledge of concepts related to Docker, AWS and Terraform

## Prerequisites

You will need an AWS account. Create one if you don't own one already. You can use free-tier resources for this test.
## Steps to Run the Project

1. Install `node and nvm`
2. Run the command `npm i`
3. Run the command `npm start`

## The Task

Provide a docker compose file for containerizing the service. 

Once docker image is built, provide the Terraform Configuration Files to create an AWS ECR repository and also a bash script to upload the docker image into the newly created ECR. 


## Submission Notes

Clone this repo. Add the Dockerfile and the Terraform Configuration files and submit a zip via email with the format "firstName.lastName-devops.challenge.zip"

Write all the necessary steps to configure and set up on a SETUP.MD file. Also provide a 1-5 minute video walkthrough using [loom](https://www.loom.com/signup). Share the Loom Link on SETUP.MD



